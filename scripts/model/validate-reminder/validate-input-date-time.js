'use strict';

const inputDate = require('./input-date');
const inputTime = require('./input-time');

const validateInputDateTime = (msg) => {
  // 日付、時刻のいずれも入力されていない場合
  if (!msg || !msg.trim()) {
    return {
      valid: false
    };
  }

  // スペース区切り(半角もしくは全角)
  const [ maybeDate, maybeTime ] = msg.split(/ |　/);

  // 日付を入力値から取得
  const inputYMD = inputDate.getYMD(maybeDate);

  // 時刻を入力値から取得
  const inputHM = inputTime.getTime(maybeTime);


  // 期待通りの入力形式の場合
  if (!!inputYMD && !!inputHM) {
    return {
      valid: true,
      date: inputYMD,
      time: inputHM
    };
  }

  // 日付が誤っている場合
  if (!!maybeDate && !inputYMD) {
    return {
      valid: false,
      invalidMessage: '日付が誤っています。'
    };
  } 

  // 時刻が入力されていない場合
  if (!maybeTime) {
    return {
      valid: false,
      invalidMessage: '時刻が入力されていません。'
    };
  }

  // 時刻が誤っている場合
  if (!!maybeTime && !inputHM) {
    return {
      valid: false,
      invalidMessage: '時刻が誤っています。'
    };
  }

  return {
    valid: false
  };
};

module.exports = validateInputDateTime;
