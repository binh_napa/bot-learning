'use strict';

const validateInputDateTime = require('./validate-input-date-time');
const validateInputMessage = require('./validate-input-message');

// リマインダー登録の入力形式バリデーション
// Confirm reminder
const validateReminder = (allMsg) => {
  const [ headMsg, ...tailMsg ] = allMsg.split(/[\n\r]/g);

  // 1行目：日付+時刻入力形式のバリデーション
  // Confirm date, input time
  const inputDateTime = validateInputDateTime(headMsg);

  const inputMessage = validateInputMessage(tailMsg);

  if (inputDateTime.valid && inputMessage.valid) {
    // 入力形式に誤りがなかった場合
    // Input no error
    return {
      valid: true,
      date: inputDateTime.date,
      time: inputDateTime.time,
      message: inputMessage.message
    };
  } else {
    // 入力形式に誤りがあった場合
    // Input error
    return {
      valid: false,
      invalidMessage: inputDateTime.invalidMessage || inputMessage.invalidMessage
    };
  }
};

module.exports = validateReminder;
