'use strict';

// 文字列を解析し、日付形式であれば年月日をObjectで返す
// Analyzing a string and return date object
class InputDate {

  getYMD(msg) {
    if (!msg) return false;


    // 入力例：6/10
    // Ex: input 6/10
    const reg = new RegExp('^([0-9]+)?/([0-9]+)?$');
    if (reg.test(msg)) {
      const [ dummy, maybeMonth, maybeDay ] = reg.exec(msg);
      if (!!maybeMonth && !!maybeDay) {
        const month = parseInt(maybeMonth, 10) || 0;
        const day   = parseInt(maybeDay, 10) || 0;

        return this.validYMD({ month, day, now: new Date() });
      }
    }

    // 該当なし
    // No apply
    return false;
  }

  checkMonth({ month }) {
    return !!((1 <= month) && (month <= 12));
  }

  checkDay({ day }) {
    return !!((1 <= day) && (day <= 31));
  }

  getYear({ month, now }) {
    const nowMonth = now.getMonth() + 1;

    // 現在[1〜3月], 指定[10〜12月]の場合、前年とみなす
    if ((nowMonth <= 3) && (10 <= month)) {
      return now.getFullYear() - 1;
    }

    // 現在[10〜12月], 指定[1〜3月]の場合、翌年とみなす
    if ((10 <= nowMonth) && (month <= 3)) {
      return now.getFullYear() + 1;
    }

    return now.getFullYear();
  }

  padZero2(num = 0) {
    return (num + 100).toString().slice(-2);
  }

  // 日付として適切であれば{ year, month, day }を返す
  // Return (year, month, day) if right day
  validYMD({ month, day, now = new Date() }) {
    if ((this.checkMonth({ month }) && (this.checkDay({ day })))) {
      const year  = this.getYear({ month, now });
      const d = new Date(`${ year }/${ this.padZero2(month) }/${ this.padZero2(day) }`);
      const valid = !/Invalid Date/i.test(d);

      if (valid) {
        return { year, month, day };
      }
    }

    return false;
  }

}

module.exports = new InputDate;
