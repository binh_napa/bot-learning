'use strict';

const validateInputMessage = (msg) => {
  // 通知内容が入力されていない場合
  // If notification content has not entered
  if (!msg || (msg.length === 0)) {
    return {
      valid: false,
      invalidMessage: '通知内容が入力されていません。'
    };
  }


  return {
    valid: true,
    message: msg.join('\n')
  };
};

module.exports = validateInputMessage;
