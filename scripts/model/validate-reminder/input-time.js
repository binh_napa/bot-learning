'use strict';

// 文字列を解析し、時刻形式であれば時刻をObjectで返す
class InputTime {

  getTime(msg) {
    if (!msg) return false;

    const [ maybeHours, maybeMinutes ] = msg.split(':');


    // 入力例：20:10
    if (!!maybeHours && !!maybeMinutes) {
      const hours = parseInt(maybeHours, 10);
      const minutes = parseInt(maybeMinutes, 10);

      return this.validTime({ hours, minutes });
    }


    // 該当なし
    return false;
  }

  checkHours({ hours }) {
    if (!hours && (hours !== 0)) return false;

    // 24時未満まで対応
    return !!((0 <= hours) && (hours < 24));
  }

  checkMinutes({ minutes }) {
    if (!minutes && (minutes !== 0)) return false;

    return !!((0 <= minutes) && (minutes <= 59));
  }

  validTime({ hours, minutes }) {
    if ((this.checkHours({ hours }) && (this.checkMinutes({ minutes })))) {
      return { hours, minutes };
    } else {
      return false;
    }
  }

}

module.exports = new InputTime;
