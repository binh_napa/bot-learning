'use strict';

const g = require('../../config');
const RESDIS_KEY = 'reminder';

// Set and get reminder
class DatastoreReminder {

  brainGet() {
    return g.robot.brain.get(RESDIS_KEY) || {};
  }

  brainSet(value) {
    return g.robot.brain.set(RESDIS_KEY, value);
  }

  save({ roomId, saveData }) {
    return this.add({ roomId, saveData });
  }

  add({ roomId, saveData }) {
    if (!roomId || !saveData) {
      g.robot.logger.info('[bug] datastoreReminder.add not found args.');
      return;
    }

    const loadData = this.brainGet();
    const sameRoomData = loadData[roomId] || [];
    const saveObject = Object.assign({}, loadData, { [ roomId ]: [ ... sameRoomData, saveData ] });
    return this.brainSet(saveObject);
  }

  load({ roomId, datetime = false }) {
    if (!roomId) {
      g.robot.logger.info('[bug] datastoreReminder.load not found args.');
      return;
    }

    const loadData = this.brainGet();
    const sameRoomData = loadData[roomId] || [];

    // Return data have roomId
    if (!datatime) {
      return Object.assign([], sameRoomData);
    }

    // Return data have roomId, datetime
    const filterSameDatetime = roomData => roomData.datetime === datetime;
    const sameDatetimeData = sameRoomData.filter(filterSameDatetime);
    return Object.assign([], sameDatetimeData);
  }

  // Get reminder information before the specified date and time
  lookforTimePassed({ datetime }) {
    if (!datetime) {
      g.robot.logger.info('[bug] datastoreReminder.lookforTimePassed not found args.');
      return;
    }

    const loadData = this.brainGet();
    const loadRooms = Object.keys(loadData);
    const filterTimePassed = (roomId, loadData) => {
      const roomReminders = loadData[roomId] || [];
      return roomReminders
        .filter(reminder => reminder.datetime <= datetime)
        .map(reminder => Object.assign({}, reminder, { roomId }));
    };

    const loadReminders = loadRooms.reduce((m, roomId) => {
      const reminders = filterTimePassed(roomId, loadData);
      return [ ...m, ...reminders ];
    }, []);

    return Object.assign([], loadReminders);
  }

  delete({ datetime }) {
    if(!datetime) {
      g.robot.logger.info('[bug] datastoreReminder.delete not found args.');
      return;
    }

    const loadData = this.brainGet();
    const loadRooms = Object.keys(loadData);
    const filterTimeAfter = (roomId, loadData) => {
      const roomReminders = loadData[roomId] || [];
      return roomReminders
        .filter(reminder => datetime < reminder.datetime);
    };

    const saveObject = loadRooms.reduce((m, roomId) => {
      const roomReminders = filterTimeAfter(roomId, loadData);
      return Object.assign({}, m, { [ roomId ] : roomReminders });
    }, {});

    return this.brainSet(saveObject);
  }
}

module.exports = new DatastoreReminder;