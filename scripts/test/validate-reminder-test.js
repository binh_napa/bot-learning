'use strict';

const assert = require('assert');
const testFunc = require('../validate-reminder');


describe('validate reminder input', () => {

  describe('誤った入力パターン', () => {
    const tests = [
      {
        input: '6/10 10:00\n会議が始まります。',
        expect: {
          valid: true,
          date: { year: 2019, month: 6, day: 10 },
          time: { hours: 10, minutes: 0 },
          message: '会議が始まります。'
        }
      },
      {
        input: '6/10 10:00',
        expect: {
          valid: false,
          invalidMessage: '通知内容が入力されていません。'
        }
      },
      {
        input: '10:00\n会議が始まります。',
        expect: {
          valid: false,
          invalidMessage: '日付が誤っています。'
        }
      },
      {
        input: '6/100 10:00\n会議が始まります。',
        expect: {
          valid: false,
          invalidMessage: '日付が誤っています。'
        }
      },
      {
        input: '6/10\n会議が始まります。',
        expect: {
          valid: false,
          invalidMessage: '時刻が入力されていません。'
        }
      },
      {
        input: '6/10 10:\n会議が始まります。',
        expect: {
          valid: false,
          invalidMessage: '時刻が誤っています。'
        }
      },
    ];

    const len = tests.length;

    for (let i = 0; i < len; i++) {

      const test = tests[i];

      it(`input > ${ test.input } / expect > ${ JSON.stringify(test.expect) }`, () => {
        assert.deepEqual(testFunc(
          test.input
        ), test.expect);
      });

    }

  });

});
