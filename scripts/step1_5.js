'use strict';

const g = require('./config');
const messageFunc = require('./common/message-func');

module.exports = (robot) => {
  g.robot = robot;
  robot.hear(/([\s\S]+)/, (res) => messageFunc(res));
}