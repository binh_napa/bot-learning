'use strict';

const g = require('../config');
const getMessage = require('../utils/get-message');
const validateReminder = require('../model/validate-reminder');

const messageFunc = (res) => {
  const msg = getMessage(res);
  if (!msg) return;

  const validMsgResult = validateReminder(msg);

  if (!validMsgResult.valid) {
    res.send(validMsgResult.invalidMessage);
  }

  const { date, time, message } = validMsgResult;
  const { year, month, day } = date;
  const { hours, minutes } = time;

  res.send([
    'Input value >>',
    ` date > ${ year } year ${ month } month ${ day } day`,
    ` time > ${ hours } : ${ minutes }`,
    ` content > ${ message }`,
  ].join('\n'));
};

module.exports = messageFunc;