'use strict';

const getMessage = (res) => {
  let msg = res.match[1];

  if(res.message.roomType === 1) {
    msg = msg.replace(/^\S+ /, '');
  }

  if (/^\{[\s\S]+\}$/.test(msg)) return false;

  if (res.message.id === null) return false;

  if ((msg.includes('stamp_set') && msg.includes('stamp_index')) ||
      (msg.includes('response') || msg.includes('question')) ||
      msg.includes('file_id')) {
    return false;
  }

  return msg;
};

module.exports = getMessage;